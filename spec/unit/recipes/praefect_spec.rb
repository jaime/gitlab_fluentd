require 'spec_helper'

describe 'gitlab_fluentd::praefect' do
  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_env'] = 'prd'
        node.normal['gitlab_fluentd']['pubsub_project'] = 'production'
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'renders the praefect config correctly' do
      expect(chef_run).to(render_file('/etc/td-agent/conf.d/praefect.conf').with_content do |content|
        expect(content).to eq(IO.read('spec/fixtures/praefect.template'))
      end)
    end
  end
end
