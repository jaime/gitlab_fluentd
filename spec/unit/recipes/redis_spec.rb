require 'spec_helper'

describe 'gitlab_fluentd::redis' do
  platform 'ubuntu', '16.04'

  normal_attributes['gitlab_fluentd']['stackdriver_enable'] = true
  normal_attributes['gitlab_fluentd']['pubsub_enable'] = true
  normal_attributes['gitlab_fluentd']['pubsub_env'] = 'prd'
  normal_attributes['gitlab_fluentd']['pubsub_project'] = 'production'
  normal_attributes['gitlab-server'] = {
    'rsyslog_client' => {
      'secrets' => {
        'backend' => 'fake_backend',
        'path' => 'fake_path',
        'key' => 'fake_key',
      },
    },
  }

  normal_attributes['omnibus-gitlab']['secrets']['all-the-omnibus-secrets'] = {
    'backend' => 'gkms',
    'path' => 'omnibus-secrets',
    'key' => 'key',
  }

  before do
    stub_command(gitlab_fluent_redis_slowlog_command).and_return(0)

    allow_any_instance_of(Chef::Recipe)
      .to receive(:get_secrets).and_return({})

    allow_any_instance_of(Chef::Recipe)
      .to receive(:get_secrets)
      .with('gkms', 'omnibus-secrets', 'key')
      .and_return(
        'omnibus-gitlab' => {
          'gitlab_rb' => {
            'redis' => {
              'password' => 'p@55w0rd!',
            },
          },
        }
      )
  end

  context 'with slowlog enabled' do
    normal_attributes['gitlab_fluentd']['redis_slowlog_enabled'] = true

    it 'renders the slowlog input in the config' do
      slowlog_input = <<~INPUT
        <source>
          @type redis_slowlog
          tag redis.slowlog
          password p@55w0rd!
        </source>
      INPUT

      expect(chef_run).to(render_file('/etc/td-agent/conf.d/redis.conf').with_content do |content|
        expect(content).to include(slowlog_input)
      end)
    end
  end
end
