require 'spec_helper'

describe 'gitlab_fluentd::postgres' do
  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
        node.normal['gitlab_fluentd']['wale_enabled'] = true
        node.normal['gitlab_fluentd']['repmgrd_enabled'] = false
      end.converge(described_recipe)
    end

    it 'creates the multiline CSV plugin' do
      expect(chef_run).to create_cookbook_file('/etc/td-agent/plugin/parser_multiline_csv.rb')
    end

    it 'creates the PostgreSQL slow log plugin' do
      expect(chef_run).to create_cookbook_file('/etc/td-agent/plugin/filter_postgresql_slowlog.rb')
    end

    it 'renders the postgres config correctly' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/postgres.conf').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/postgres.template'))
      }
    end
  end
end
