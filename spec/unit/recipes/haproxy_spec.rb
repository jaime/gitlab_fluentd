describe 'gitlab_fluentd::haproxy' do
  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['prometheus']['labels'] = {
          'stage' => 'main',
          'shard' => 'default',
          'type' => 'lb',
        }
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to_not include 'enable_monitoring true'
      }
    end

    it 'sets the buffer_chunk_limit' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'buffer_chunk_limit 8m'
      }
    end

    it 'renders the haproxy config correctly' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/haproxy.template'))
      }
    end
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab_fluentd']['buffer_chunk_limit'] = '7m'
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'sets the buffer_chunk_limit' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'buffer_chunk_limit 7m'
      }
    end
  end

  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab_fluentd']['google_cloud_monitoring'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/haproxy.conf').with_content { |content|
        expect(content).to include 'enable_monitoring true'
      }
    end
  end
end
