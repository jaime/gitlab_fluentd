require 'chefspec'
require 'chefspec/berkshelf'

module Gitlab
  module FluentSpecHelper
    def gcloud_command
      "/usr/sbin/td-agent-gem list '^gcloud$' -i -v 0.24.1"
    end

    def googleauth_command
      "/usr/sbin/td-agent-gem list '^googleauth$' -i -v 0.13.0"
    end

    def google_cloud_command
      "/usr/sbin/td-agent-gem list '^google-cloud$' -i -v 0.64.0"
    end

    def google_cloud_env_command
      "/usr/sbin/td-agent-gem list '^google-cloud-env$' -i -v 1.3.2"
    end

    def fluent_plugin_google_cloud_command
      "/usr/sbin/td-agent-gem list '^fluent-plugin-google-cloud$' -i -v 0.8.7"
    end

    def fluent_plugin_multi_format_parser_command
      "/usr/sbin/td-agent-gem list '^fluent-plugin-multi-format-parser$' -i -v 1.0.0"
    end

    def gitlab_fluent_redis_slowlog_command
      "/usr/sbin/td-agent-gem list '^gitlab-fluent-plugin-redis-slowlog$' -i -v 0.1.2"
    end
  end
end

RSpec.configure do |config|
  config.include Gitlab::FluentSpecHelper

  config.before(:each) do
    # Stub common gem installations
    stub_command(gcloud_command).and_return(0)
    stub_command(googleauth_command).and_return(0)
    stub_command(google_cloud_command).and_return(0)
    stub_command(google_cloud_env_command).and_return(0)
    stub_command(fluent_plugin_google_cloud_command).and_return(0)
    stub_command(fluent_plugin_multi_format_parser_command).and_return(0)

    # Stub common secrets
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('elastic_cloud_host' => '1.2.3.4',
                  'elastic_cloud_port' => '1234',
                  'elastic_cloud_user' => 'root',
                  'elastic_cloud_password' => 'toor',
                  'pubsub_key' => '{"foo": "bar"}')
  end
end
