# Cookbook:: gitlab_fluentd
# Recipe:: postgres
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

cookbook_file '/etc/td-agent/plugin/parser_multiline_csv.rb' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

cookbook_file '/etc/td-agent/plugin/filter_postgresql_slowlog.rb' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'postgres.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
