# Cookbook:: gitlab_fluentd
# Recipe:: redis
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.
include_recipe 'gitlab_fluentd::default'

redis_secrets = omnibus_secrets['omnibus-gitlab']['gitlab_rb']['redis']

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'redis.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  variables(redis_password: redis_secrets['password'])
  notifies :restart, 'service[td-agent]', :delayed
end

if node['gitlab_fluentd']['redis_slowlog_enabled']
  slowlog_input_version = node['gitlab_fluentd']['gem_versions']['gitlab-fluent-plugin-redis-slowlog']
  execute 'install gitlab-fluent-plugin-redis-slowlog gem' do
    command "/usr/sbin/td-agent-gem install gitlab-fluent-plugin-redis-slowlog --no-document -v #{slowlog_input_version}"
    notifies :restart, 'service[td-agent]', :delayed
    not_if "/usr/sbin/td-agent-gem list '^gitlab-fluent-plugin-redis-slowlog$' -i -v #{slowlog_input_version}"
  end
end
