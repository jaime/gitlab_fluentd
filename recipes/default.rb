# Cookbook:: gitlab_fluentd
# Recipe:: default
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

# Fetch secrets from gitlab_server

if node['gitlab-server']['rsyslog_client'].attribute?('secrets')
  elastic_cloud_conf = get_secrets(node['gitlab-server']['rsyslog_client']['secrets']['backend'],
                                   node['gitlab-server']['rsyslog_client']['secrets']['path'],
                                   node['gitlab-server']['rsyslog_client']['secrets']['key'])
  node.override['gitlab_fluentd']['pubsub_key'] = elastic_cloud_conf['pubsub_key']
end

apt_repository 'fluentd' do
  uri           node['gitlab_fluentd']['apt_repo_uri']
  distribution  node['gitlab_fluentd']['apt_repo_distribution']
  components    node['gitlab_fluentd']['apt_repo_components']
  key           node['gitlab_fluentd']['apt_repo_key']
end

apt_package 'build-essential'

apt_package 'td-agent' do
  version node['gitlab_fluentd']['td-agent']['version']
  notifies :run, 'execute[install multi-format gem]', :immediately
end

directory node['gitlab_fluentd']['config_dir_modules'] do
  mode '0755'
  owner 'root'
  group 'root'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

managed_directory node['gitlab_fluentd']['config_dir_modules'] do
  action :clean
  notifies :restart, 'service[td-agent]', :delayed
end

directory node['gitlab_fluentd']['es_template_dir'] do
  mode '0755'
  owner 'root'
  group 'root'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

directory node['gitlab_fluentd']['log_dir'] do
  mode '0755'
  owner 'td-agent'
  group 'td-agent'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

environment_vars = %w(
  Environment=LD_PRELOAD=/opt/td-agent/embedded/lib/libjemalloc.so
  Environment=GEM_HOME=/opt/td-agent/embedded/lib/ruby/gems/2.4.0/
  Environment=GEM_PATH=/opt/td-agent/embedded/lib/ruby/gems/2.4.0/
  Environment=FLUENT_CONF=/etc/td-agent/td-agent.conf
  Environment=FLUENT_PLUGIN=/etc/td-agent/plugin
  Environment=FLUENT_SOCKET=/var/run/td-agent/td-agent.sock
)

environment_vars.push('Environment=GOOGLE_APPLICATION_CREDENTIALS=/etc/td-agent/pubsub-gcp-key.json') if node['gitlab_fluentd']['pubsub_key']

systemd_unit 'td-agent.service' do
  content <<-EOU.gsub(/^\s+/, '')
    [Unit]
    Description=td-agent: Fluentd based data collector for Treasure Data
    Documentation=https://docs.treasuredata.com/articles/td-agent
    After=network-online.target
    Wants=network-online.target

    [Service]
    User=root
    Group=root
    LimitNOFILE=65536
    #{environment_vars.join("\n")}
    PIDFile=/var/run/td-agent/td-agent.pid
    RuntimeDirectory=td-agent
    Type=forking
    ExecStart=/opt/td-agent/embedded/bin/fluentd --log /var/log/td-agent/td-agent.log --daemon /var/run/td-agent/td-agent.pid
    ExecStop=/bin/kill -TERM ${MAINPID}
    ExecReload=/bin/kill -HUP ${MAINPID}
    Restart=always
    TimeoutStopSec=120

    [Install]
    WantedBy=multi-user.target
  EOU

  action %i(create)
  notifies :restart, 'service[td-agent]', :delayed
end

template '/etc/td-agent/td-agent.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'sessions.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

file node['gitlab_fluentd']['pubsub_file'] do
  owner 'root'
  group 'root'
  mode '0600'
  content node['gitlab_fluentd']['pubsub_key']
  notifies :restart, 'service[td-agent]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_key'].nil? }
end

cookbook_file '/etc/td-agent/plugin/out_cloud_pubsub.rb' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :run, 'execute[install googleauth gem]', :immediately
  notifies :run, 'execute[install gcloud gem]', :immediately
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
end

cookbook_file '/etc/td-agent/prometheus-mixin.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

file '/etc/td-agent/plugin/out_cloud_pubsub.rb' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_enable'] }
end

# old plugin name, deprecated but we ensure it is
# deleted if it still exists.
file '/etc/td-agent/plugin/plugin-cloud-pubsub.rb' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

## Install Gems

gem_versions = node['gitlab_fluentd']['gem_versions']

execute 'install googleauth gem' do
  command "/usr/sbin/td-agent-gem install googleauth --no-document -v #{gem_versions['googleauth']}"
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
  not_if "/usr/sbin/td-agent-gem list '^googleauth$' -i -v #{gem_versions['googleauth']}"
end

execute 'install fluent-plugin-google-cloud gem' do
  command "/usr/sbin/td-agent-gem install fluent-plugin-google-cloud --no-document -v #{gem_versions['fluent-plugin-google-cloud']}"
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['stackdriver_enable'] }
  not_if "/usr/sbin/td-agent-gem list '^fluent-plugin-google-cloud$' -i -v #{gem_versions['fluent-plugin-google-cloud']}"
end

execute 'install gcloud gem' do
  command "/usr/sbin/td-agent-gem install gcloud --no-document -v #{gem_versions['gcloud']}"
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
  not_if "/usr/sbin/td-agent-gem list '^gcloud$' -i -v #{gem_versions['gcloud']}"
end

execute 'install google-cloud gem' do
  command '/usr/sbin/td-agent-gem install google-cloud google-cloud-bigquery google-cloud-dns google-cloud-resource_manager google-cloud-storage google-cloud-translate --no-document'
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
  not_if "/usr/sbin/td-agent-gem list '^google-cloud$' -i -v #{gem_versions['google-cloud']}"
end

execute 'install google-cloud-env gem' do
  command "/usr/sbin/td-agent-gem install google-cloud-env --no-document -v #{gem_versions['google-cloud-env']}"
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
  not_if "/usr/sbin/td-agent-gem list '^google-cloud-env$' -i -v #{gem_versions['google-cloud-env']}"
end

execute 'install multi-format gem' do
  command "/usr/sbin/td-agent-gem install fluent-plugin-multi-format-parser --no-document -v #{gem_versions['fluent-plugin-multi-format-parser']}"
  notifies :restart, 'service[td-agent]', :delayed
  not_if "/usr/sbin/td-agent-gem list '^fluent-plugin-multi-format-parser$' -i -v #{gem_versions['fluent-plugin-multi-format-parser']}"
end

# td-agent is started in its package's postinst.
service 'td-agent' do
  action :enable
end

logrotate_app 'td-agent' do
  path '/var/log/td-agent/td-agent.log'
  options %w(missingok compress delaycompress notifempty)
  rotate 7
  frequency 'daily'
  size '1G'
  postrotate <<-EOF
pid=/var/run/td-agent/td-agent.pid
    if [ -s "$pid" ]
    then
      kill -USR1 "$(cat $pid)"
    fi
  EOF
end
