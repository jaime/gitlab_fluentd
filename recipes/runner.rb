# Cookbook:: gitlab_fluentd
# Recipe:: runner
# License:: MIT
#
# Copyright:: 2019, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'runner.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
