# Cookbook:: gitlab_fluentd
# Recipe:: grafana
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'
include_recipe 'gitlab_fluentd::monitoring'
