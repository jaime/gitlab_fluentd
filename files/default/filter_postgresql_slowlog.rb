# frozen_string_literal: true

require 'fluent/plugin/filter'

module Fluent
  module Plugin
    # Filters PostgreSQL slow log duration and statements from parsed record.
    #
    # Examples:
    # duration: 2357.1 ms  execute <unnamed>: SELECT * FROM projects
    # duration: 1873.345 ms  execute <unnamed>: SELECT COUNT(*) FROM "projects" /*application:sidekiq,correlation_id:d67cae54c169e0cab7d73389e2934f0e,jid:52a1c8a9e4c555ea573f20f0,job_class:Geo::MetricsUpdateWorker*/
    class PostgreSQLSlowLog < Filter
      Fluent::Plugin.register_filter('postgresql_slowlog', self)

      SLOWLOG_REGEXP = /^duration: (\d+(?:\.\d+)?) ms .*?:\s*(.*)/m.freeze

      def filter(_tag, _time, record)
        return record unless record.key?('message')

        # rubocop:disable Style/PerlBackrefs
        if record['message'] =~ SLOWLOG_REGEXP
          record['duration_ms'] = $1.to_f
          record['statement'] = $2
        end
        # rubocop:enable Style/PerlBackrefs

        record
      end
    end
  end
end
