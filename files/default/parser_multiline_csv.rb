# frozen_string_literal: true

require 'fluent/plugin/parser_csv'

module Fluent
  module Plugin
    # This class can be used to parse CSV files that span multiple lines.
    # Like the [multiline parser](https://docs.fluentd.org/parser/multiline),
    # define a `format_firstline` for the `tail` input module to match against.
    # Then use the [csv parser](https://docs.fluentd.org/parser/csv) parameters.
    class MultilineCSVParser < CSVParser
      Plugin.register_parser('multiline_csv', self)

      desc 'Specify regexp pattern for start line of multiple lines'
      config_param :format_firstline, :string, default: nil

      def configure(conf)
        super

        if @format_firstline
          check_format_regexp(@format_firstline, 'format_firstline')
          @firstline_regex = Regexp.new(@format_firstline[1..-2])
        end
      end

      # Used by in_tail:
      # https://github.com/fluent/fluentd/blob/47be96e3a98fa247b59e479f7c62bfeff1a9ec55/lib/fluent/plugin/in_tail.rb#L523
      def has_firstline?
        !!@format_firstline
      end

      # Used by in_tail:
      # https://github.com/fluent/fluentd/blob/47be96e3a98fa247b59e479f7c62bfeff1a9ec55/lib/fluent/plugin/in_tail.rb#L526
      def firstline?(text)
        @firstline_regex.match(text)
      end

      private

      def check_format_regexp(format, key)
        if format[0] == '/' && format[-1] == '/'
          begin
            Regexp.new(format[1..-2])
          rescue => e
            raise ConfigError, "Invalid regexp in #{key}: #{e}"
          end
        else
          raise ConfigError, "format_firstline should be Regexp, need //: '#{format}'"
        end
      end
    end
  end
end
