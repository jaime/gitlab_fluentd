# Module to fetch all omnibus secrets

module OmnibusSecrets
  def omnibus_secrets
    secrets_config = node['omnibus-gitlab']['secrets']['all-the-omnibus-secrets']
    get_secrets(secrets_config['backend'], secrets_config['path'], secrets_config['key'])
  end
end

Chef::DSL::Recipe.include OmnibusSecrets
