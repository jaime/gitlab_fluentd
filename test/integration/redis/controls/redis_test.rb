# frozen_string_literal: true

# InSpec tests for recipe gitlab_fluentd::redis

control 'slowlog output' do
  impact 1.0
  title 'Test for the config rendered'

  config = JSON.parse(File.read(File.join(File.dirname(__FILE__), '../../data_bags/secrets/omnibus-secrets.json')))
  redis_password = config['omnibus-gitlab']['gitlab_rb']['redis']['password']

  describe slowlog_output(redis_password) do
    it { should exist }
    it { should have_key('exec_time_s') }
    it { should_not have_key('exec_time') }
    its(:exec_time_s) { should be_a(Float) }
    it { should be_cleanup }
  end
end
