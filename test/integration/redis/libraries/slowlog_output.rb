# frozen_string_literal: true

class SlowlogOutput < Inspec.resource(1)
  name 'slowlog_output'

  def initialize(redis_password)
    @original_config = load_original_config
    @redis_password = redis_password

    reconfigure_td_agent
    read_new_slowlog_entry
  end

  def exists?
    inspec.directory(resource_config[:output_path]).exist? && slowlog.exist?
  end

  def has_key?(key)
    last_slowlog_entry[key]
  end

  def exec_time_s
    last_slowlog_entry['exec_time_s']
  end

  def cleanup?
    run("echo '#{original_config}' > '#{resource_config[:config_path]}'")

    if inspec.file(resource_config[:config_path]).content != original_config
      fail_resource('Failed to restor the config')
    end
  end

  private

  attr_reader :original_config, :redis_password
  attr_accessor :last_slowlog_entry

  def resource_config
    @resource_config ||= {
      config_path: '/etc/td-agent/conf.d/redis.conf',
      output_path: '/tmp/inspec/redis-slowlog-output',
      slowlog_path: '/tmp/inspec/slowlog.log',
    }
  end

  def load_original_config
    content = inspec.file(resource_config[:config_path]).content

    if content.to_s.empty?
      fail_resource("Unable to read current #{resource_config[:config_path]}")
    end

    content
  end

  def output_config
    @output_config ||= <<~OUT
      <store redis.slowlog>
        @type file
        path #{resource_config[:output_path]}
        symlink_path #{resource_config[:slowlog_path]}
        append true
        <buffer>
          @type file
          timekey 1h
          timekey_wait 1s
        </buffer>
        <format>
          @type json
        </format>
      </store>
    OUT
  end

  def reconfigure_td_agent
    return if original_config.include?(output_config)

    new_config = original_config.sub(%r{</match>}, "#{output_config}\n</match>")

    run("echo '#{new_config}' > '#{resource_config[:config_path]}'")
    written_config = inspec.file(resource_config[:config_path]).content

    unless written_config.include?(output_config)
      fail_resource('Failed to create test config')
    end

    restart_td_agent
  end

  def restart_td_agent
    run('systemctl restart td-agent')
    # Wait until td-agent is running
    unless wait_until { inspec.service('td-agent').running? }
      fail_resource('td-agent failed to restart')
    end
  end

  def slow_command
    # A slow eval
    "EVAL 'local a=0; while(a < 999999) do a = a+1 end' 0"
  end

  def run_slow_command
    run("echo \"#{slow_command}\" | redis-cli -x -a '#{redis_password}'")
  end

  def run(command)
    result = inspec.bash(command)

    fail_resource("failed to run #{command}") if result.exit_status != 0

    result
  end

  def read_last_slowlog_entry
    last_line = slowlog.content&.lines&.last
    JSON.parse(last_line) if last_line
  rescue JSON::ParserError => e
    fail_resource("Could not parse slowlog: #{e.message}")
  end

  def read_new_slowlog_entry
    new_entry = wait_until do
      run_slow_command
      read_entry = read_last_slowlog_entry
      break read_entry if read_entry != last_slowlog_entry
    end

    unless new_entry
      fail_resource('Failed to read new entry from the slowlog in time')
    end

    self.last_slowlog_entry = new_entry
  end

  def wait_until
    deadline = Time.now + 60

    result = yield until result || deadline < Time.now

    result
  end

  def slowlog
    inspec.file(resource_config[:slowlog_path])
  end
end
