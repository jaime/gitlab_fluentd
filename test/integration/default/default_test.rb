# frozen_string_literal: true

# InSpec tests for recipe gitlab_fluentd::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab_fluentd cookbook'
  desc '
    This control ensures that:
      * td-agent is running'

  describe service('td-agent') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

control 'logrotate-config-valid' do
  impact 1.0
  title 'apt supplied config is overwritten'

  describe file('/etc/logrotate.d/td-agent') do
    it { should exist }
    its('content') { should include 'daily' }
    its('content') { should include 'rotate 7' }
    its('content') { should include 'size 1G' }
    its('content') { should include 'postrotate' }
    its('content') { should include 'endscript' }
  end
end
