# apt repo settings
default['gitlab_fluentd']['apt_repo_platform'] = node['platform']
default['gitlab_fluentd']['apt_repo_distribution'] = node['lsb']['codename']
default['gitlab_fluentd']['apt_repo_uri'] = "http://packages.treasuredata.com/3/#{node['gitlab_fluentd']['apt_repo_platform']}/#{node['gitlab_fluentd']['apt_repo_distribution']}"
default['gitlab_fluentd']['apt_repo_components'] = ['contrib']
default['gitlab_fluentd']['apt_repo_key'] = 'https://packages.treasuredata.com/GPG-KEY-td-agent'

default['gitlab_fluentd']['td-agent']['version'] = '3.8.0-0'

default['gitlab_fluentd']['buffer_chunk_limit'] = '8m'

# elasticsearch output settings
default['gitlab_fluentd']['stackdriver_enable'] = false

# pubsub output settings
default['gitlab_fluentd']['google_cloud_monitoring'] = false
default['gitlab_fluentd']['pubsub_enable'] = false
default['gitlab_fluentd']['pubsub_enable_nginx'] = false
default['gitlab_fluentd']['pubsub_enable_haproxy'] = false
default['gitlab_fluentd']['pubsub_env'] = ''
default['gitlab_fluentd']['pubsub_project'] = ''
default['gitlab_fluentd']['pubsub_key'] = nil
default['gitlab_fluentd']['pubsub_file'] = '/etc/td-agent/pubsub-gcp-key.json'
# Override the environment that is set in fluentd,
# defaults to the node's env.
default['gitlab_fluentd']['pubsub_log_env'] = nil

# directory settings
default['gitlab_fluentd']['config_dir_modules'] = '/etc/td-agent/conf.d'
default['gitlab_fluentd']['es_template_dir'] = '/etc/td-agent/es-templates'
default['gitlab_fluentd']['log_dir'] = '/var/log/td-agent'

# Postgres settings
default['gitlab_fluentd']['enable_postgres_csvlog_ingestion'] = true
default['gitlab_fluentd']['postgres_log_path'] = '/var/log/gitlab/postgresql/current'
default['gitlab_fluentd']['postgres_csvlog_path'] = '/var/log/gitlab/postgresql/*.csv'
default['gitlab_fluentd']['pgbouncer_log_path'] = '/var/log/gitlab/pgbouncer/current'
default['gitlab_fluentd']['walg_log_archive_path'] = '/var/log/wal-g/wal-g.log'
default['gitlab_fluentd']['walg_log_basebackup_path'] = '/var/log/wal-g/wal-g_backup_push.log'
default['gitlab_fluentd']['repmgrd_enabled'] = true
default['gitlab_fluentd']['wale_enabled'] = false

default['gitlab_fluentd']['gem_versions']['fluent-plugin-google-cloud'] = '0.8.7'
default['gitlab_fluentd']['gem_versions']['fluent-plugin-multi-format-parser'] = '1.0.0'
default['gitlab_fluentd']['gem_versions']['gcloud'] = '0.24.1'
default['gitlab_fluentd']['gem_versions']['googleauth'] = '0.13.0'
default['gitlab_fluentd']['gem_versions']['google-cloud'] = '0.64.0'
default['gitlab_fluentd']['gem_versions']['google-cloud-env'] = '1.3.2'

default['gitlab_fluentd']['gem_versions']['gitlab-fluent-plugin-redis-slowlog'] = '0.1.2'
default['gitlab_fluentd']['redis_slowlog_enabled'] = false

default['gitlab_fluentd']['log_parsing_format'] = 'plain'
